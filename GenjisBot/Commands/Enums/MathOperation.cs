﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenjisBot
{
    public enum MathOperation
    {
        Add,
        Subtract,
        Multiply,
        Divide,
        Modulo
    }
}
