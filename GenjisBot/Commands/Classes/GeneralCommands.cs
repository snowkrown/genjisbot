﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GenjisBot
{
    [Description("Comandos generales de Genji")]
    public class GeneralCommands
    {
        private GenjiSongController songsController;

        private GenjiSongController SongsController
        {
            get
            {
                if (songsController == null)
                    songsController = new GenjiSongController();

                return songsController;
            }
        }

        [Command("dance")]
        [Description("Gif de baile.")]
        public async Task Dance(CommandContext ctx)
        {
            DiscordEmbed danceEmbed = new DiscordEmbedBuilder
            {
                ImageUrl = "https://vignette.wikia.nocookie.net/overwatch/images/5/5a/Genji_dance.gif"
            };

            await ctx.RespondAsync(null, false, danceEmbed);
        }

        [Command("ping")]
        [Description("Hace Ping para saber si Genji's Bot está activo")]
        public async Task Ping(CommandContext ctx)
        {
            var emoji = DiscordEmoji.FromName(ctx.Client, ":ping_pong:");
            await ctx.TriggerTypingAsync();
            await Task.Delay(500);
            await ctx.RespondAsync($"{emoji} Pong!\n{ctx.Client.Ping}ms");
        }

        [Command("greet"), Description("Saludar al usuario."), Aliases("sayhi", "say_hi", "greetings", "hello", "hi", "hola", "saludos", "yo", "hi_there", "hey")]
        public async Task Greet(CommandContext ctx, [Description("Usuario a saludar.")] DiscordMember member = null)
        {
            var emoji = DiscordEmoji.FromName(ctx.Client, ":wave:");
            await ctx.TriggerTypingAsync();
            await Task.Delay(500);

            if (member != null)
                await ctx.RespondAsync($"{emoji} ¡Hola, {member.Username}!");
            else
                await ctx.RespondAsync($"{emoji} ¡Hola, {ctx.Message.Author.Mention}!");
        }

        [Command("love"), Aliases("inlove") , Description("Gif de Genji con Angela")]
        public async Task Inlove(CommandContext ctx)
        {
            var emoji = DiscordEmoji.FromName(ctx.Client, ":heart_eyes:");
            await ctx.TriggerTypingAsync();
            await Task.Delay(500);
            await ctx.RespondAsync($"{emoji} {emoji} {emoji}");
        }

        [Command("angela"), Aliases("mercy"), Description("Gif de Genji con Angela")]
        public async Task Mercy(CommandContext ctx)
        {
            var emoji = DiscordEmoji.FromName(ctx.Client, ":heart_eyes:");
            DiscordEmbed loveEmbed = new DiscordEmbedBuilder
            {
                ImageUrl = "https://data.whicdn.com/images/261671849/original.gif"
            };

            await ctx.TriggerTypingAsync();
            await Task.Delay(500);
            await ctx.RespondAsync($"{emoji} {emoji} {emoji}", false, loveEmbed);
        }

        [Command("kawai")]
        [Description("Gif Kawai.")]
        public async Task Kawai(CommandContext ctx)
        {
            DiscordEmbed kawaiEmbed = new DiscordEmbedBuilder
            {
                ImageUrl = "http://pa1.narvii.com/6402/577f5434829962aeb8d3433e040c26078ab41f2d_00.gif"
            };
            await ctx.RespondAsync(null, false, kawaiEmbed);
        }

        [Command("rsong"), Aliases("randomsong", "songreco")]
        [Description("Genji recomienda una canción aleatoria.")]
        public async Task RandomSong(CommandContext ctx)
        {
            await SongsController.Initialize();
            await ctx.RespondAsync(null, false, SongsController.GetRandomSongEmbed());
        }
    }
}
