﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Newtonsoft.Json;

namespace GenjisBot
{
    public class CustomResponseCommands
    {
        private List<Response> allResponses = new List<Response>();

        [Command("isay")]
        [Description("Asigna una respuesta a una frase dicha por quien usa el comando.")]
        public async Task WhenISay(CommandContext ctx, [Description("Frase")]string phrase, [Description("Respuesta")]string response)
        {
            await ctx.TriggerTypingAsync();
            await Task.Delay(500);

            if (!string.IsNullOrEmpty(phrase) && !string.IsNullOrEmpty(response))
                await TrySavePhraseToDatabase(ctx, phrase, response, ctx.Message.Author.Id);
            else
                await ctx.RespondAsync($"No puedes enviar parámetros vacíos.");
        }

        [Command("say")]
        [Description("Asigna una respuesta a una frase dicha por un usuario.")]
        public async Task WhenXSay(CommandContext ctx, [Description("Frase")]string phrase, [Description("Respuesta")]string response, [Description("Usuario que activará la respuesta.")] DiscordMember member = null)
        {
            ulong memberId = member == null ? 0 : member.Id;
            await ctx.TriggerTypingAsync();
            await Task.Delay(500);

            if (!string.IsNullOrEmpty(phrase) && !string.IsNullOrEmpty(response))
                await TrySavePhraseToDatabase(ctx, phrase, response, memberId);
            else
                await ctx.RespondAsync($"No puedes enviar parámetros vacíos.");
        }

        private async Task TrySavePhraseToDatabase(CommandContext ctx, string phrase, string response, ulong memberId)
        {
            try
            {
                await TrySavePhrase(ctx, phrase, response, memberId);
            }
            catch (Exception e)
            {
                Console.Write("Error " + e);
                await ctx.RespondAsync($"La frase o palabra '{phrase}' no pudo ser asignada.\nEs posible que ya haya sido registrada.");
            }
        }

        private async Task TrySavePhrase(CommandContext ctx, string phrase, string response, ulong memberId)
        {
            await SavePhrase(phrase, response, memberId);

            if (memberId > 0)
                await ctx.RespondAsync($"Cuando {(await ctx.Client.GetUserAsync(memberId)).Mention} diga '{phrase}', responderé '{response}'");
            else
                await ctx.RespondAsync($"Cuando cualquiera diga '{phrase}', responderé '{response}'");
        }

        private async Task SavePhrase(string phrase, string response, ulong memberId)
        {
            await GetResponses();

            if (allResponses.Exists(p => p.phrase == phrase))
                throw new System.Exception ($"The phrase {phrase} is already taken.");

            Response newResponse = new Response
            {
                activatorId = memberId,
                phrase = phrase,
                replace = response
            };

            allResponses.Add(newResponse);
            await SaveResponses();
        }

        public async Task GetResponses()
        {
            if (allResponses.Count > 0)
                await Task.CompletedTask;

            string json = "";

            using (var fs = File.OpenRead("Custom Responses/Words Database/ResponsesDatabase.json"))
            {
                using (var sr = new StreamReader(fs, new UTF8Encoding(false)))
                    json = await sr.ReadToEndAsync();
            }

            allResponses = JsonConvert.DeserializeObject<List<Response>>(json);
        }

        public async Task SaveResponses()
        {
            string json = JsonConvert.SerializeObject(allResponses.ToArray());

            using (var fs = File.OpenWrite("Custom Responses/Words Database/ResponsesDatabase.json"))
            {
                using (var sr = new StreamWriter(fs, new UTF8Encoding(false)))
                    await sr.WriteAsync(json);
            }
        }
    }
}