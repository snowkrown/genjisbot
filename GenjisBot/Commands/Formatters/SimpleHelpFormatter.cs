﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Converters;
using DSharpPlus.CommandsNext.Entities;

namespace GenjisBot
{
    public class SimpleHelpFormatter : IHelpFormatter
    {
        private StringBuilder MessageBuilder { get; }

        public SimpleHelpFormatter()
        {
            MessageBuilder = new StringBuilder();
        }
        
        public IHelpFormatter WithCommandName(string name)
        {
            MessageBuilder.Append(":arrow_forward: *Comando*: ").AppendLine(Formatter.Bold(name)).AppendLine();
            return this;
        }
        
        public IHelpFormatter WithDescription(string description)
        {
            MessageBuilder.Append(":abc: *Descripción*: ").AppendLine(description).AppendLine();
            return this;
        }

        public IHelpFormatter WithGroupExecutable()
        {
            MessageBuilder.AppendLine(Formatter.Italic ("This group is a standalone command.")).AppendLine();
            return this;
        }
        
        public IHelpFormatter WithAliases(IEnumerable<string> aliases)
        {
            MessageBuilder.Append(":open_file_folder: *Alias*:\n     :beginner: ")
                .AppendLine(string.Join("\n     :beginner: ", aliases)).AppendLine();
            return this;
        }

        public IHelpFormatter WithArguments(IEnumerable<CommandArgument> arguments)
        {
            this.MessageBuilder.Append(":open_file_folder: *Argumentos*:")
                .AppendLine()
                .AppendLine(string.Join("\n", arguments.Select(xarg => "     :beginner: " + $"{xarg.Name} ({xarg.Type.ToUserFriendlyName()})"))).AppendLine();

            return this;
        }
        
        public IHelpFormatter WithSubcommands(IEnumerable<Command> subcommands)
        {
            MessageBuilder.Append(":open_file_folder: *Subcomandos*:")
                .AppendLine()
                .AppendLine(string.Join("\n", subcommands.Select(xc => Formatter.Bold("     :beginner: " + xc.Name))));

            return this;
        }
        
        public CommandHelpMessage Build()
        {
            return new CommandHelpMessage(this.MessageBuilder.ToString().Replace("\r\n", "\n"));
        }
    }
}
