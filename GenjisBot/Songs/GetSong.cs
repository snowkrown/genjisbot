﻿using DSharpPlus.Entities;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System;
using System.Threading.Tasks;

namespace GenjisBot
{
    public class GenjiSong
    {
        public string name;
        public string subtle;
        public string img;
    }

    public class GenjiSongController
    {
        private List<GenjiSong> allSongs = new List<GenjiSong>();

        public async Task Initialize ()
        {
            if (allSongs.Count > 0)
                await Task.CompletedTask;

            string json = "";

            using (var fs = File.OpenRead("Songs/songs.json"))
            {
                using (var sr = new StreamReader(fs, new UTF8Encoding(false)))
                    json = await sr.ReadToEndAsync();
            }

            allSongs = JsonConvert.DeserializeObject<List<GenjiSong>>(json);
        }

        public DiscordEmbed GetRandomSongEmbed()
        {
            GenjiSong song = allSongs[new Random().Next(0, allSongs.Count)];

            DiscordEmbed embed = new DiscordEmbedBuilder
            {
                Title = song.subtle,
                //ThumbnailUrl = "https://freeiconshop.com/wp-content/uploads/edd/youtube-flat.png",
                ImageUrl = song.img,
                Author = new DiscordEmbedBuilder.EmbedAuthor
                {
                    Name = song.name,
                    Url = GetSongURL(song.name, song.subtle)
                },
                Url = GetSongURL(song.name, song.subtle),
            };

            return embed;
        }

        private string GetSongURL(string name, string subtle)
        {
            string url = "http://www.google.com/search?q=";
            url += (name.Replace(" ", "+") + "+" + subtle.Replace(" ", "+"));
            url += "&site:www.youtube.com";
            return url + "&btnI=I";
        }

        private string GetWikiAuthorURL(string subtle)
        {
            string url = "http://www.google.com/search?q=";
            url += subtle.Replace(" ", "+");
            url += "&site:www.wikipedia.com";
            return url + "&&btnI=I";
        }
    }
}
