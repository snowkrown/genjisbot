﻿using DSharpPlus;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus.Net.WebSocket;
using DSharpPlus.EventArgs;
using DSharpPlus.Entities;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Exceptions;
using DSharpPlus.Interactivity;
using System.Collections.Generic;

namespace GenjisBot
{
    class Program
    {
        public DiscordClient Client { get; set; }
        public CommandsNextModule Commands { get; set; }
        public CustomResponses CustomResponses { get; set; }

        static void Main(string[] args)
        {
            var prog = new Program();
            prog.RunBotAsync().GetAwaiter().GetResult();
        }

        public async Task RunBotAsync()
        {
            ConfigJson cfgjson = GetConfigJson().Result;
            CreateNewClient(GetDiscordConfiguration(cfgjson));
            SetUpCommands(cfgjson);
            InitializeInteractivityModule();
            await Client.ConnectAsync();
            await Task.Delay(-1);
        }

        private void InitializeInteractivityModule()
        {
            Client.UseInteractivity(new InteractivityConfiguration
            {
                PaginationBehaviour = TimeoutBehaviour.Ignore,
                PaginationTimeout = TimeSpan.FromMinutes(5),
                Timeout = TimeSpan.FromMinutes(2)
            });
        }

        private async Task<ConfigJson> GetConfigJson ()
        {
            string json = "";

            using (var fs = File.OpenRead("Config/config.json"))
            {
                using (var sr = new StreamReader(fs, new UTF8Encoding(false)))
                    json = await sr.ReadToEndAsync();
            }

            return JsonConvert.DeserializeObject<ConfigJson>(json);
        }

        private DiscordConfiguration GetDiscordConfiguration(ConfigJson cfgjson)
        {
            DiscordConfiguration cfg = new DiscordConfiguration
            {
                Token = cfgjson.Token,
                TokenType = TokenType.Bot,
                AutoReconnect = true,
                LogLevel = LogLevel.Debug,
                UseInternalLogHandler = true
            };

            return cfg;
        }

        private void CreateNewClient(DiscordConfiguration cfg)
        {
            Client = new DiscordClient(cfg);
            Client.Ready += Client_Ready;
            Client.GuildAvailable += Client_GuildAvailable;
            Client.ClientErrored += Client_ClientError;
            Client.MessageCreated += Client_MessageCreated;
            CustomResponses = new CustomResponses(Client);
        }

        private void SetUpCommands(ConfigJson cfgjson)
        {
            Commands = Client.UseCommandsNext(GetCommandsConfig(cfgjson));
            AddCommandsListeners();
            RegisterConverters();
            RegisterCommands();
        }

        private void AddCommandsListeners()
        {
            Commands.CommandExecuted += Commands_CommandExecuted;
            Commands.CommandErrored += Commands_CommandErrored;
        }

        private CommandsNextConfiguration GetCommandsConfig (ConfigJson cfgjson)
        {
            CommandsNextConfiguration ccfg = new CommandsNextConfiguration
            {
                StringPrefix = cfgjson.CommandPrefix,
                EnableDms = true,
                EnableMentionPrefix = true,
                CaseSensitive = false
            };

            return ccfg;
        }

        private void RegisterConverters()
        {
            var mathopcvt = new MathOperationConverter();
            CommandsNextUtilities.RegisterConverter(mathopcvt);
            CommandsNextUtilities.RegisterUserFriendlyTypeName<MathOperation>("operation");
        }

        private void RegisterCommands()
        {
            Commands.RegisterCommands<GeneralCommands>();
            Commands.RegisterCommands<CustomResponseCommands>();
            Commands.SetHelpFormatter<SimpleHelpFormatter>();
        }

        private async Task Client_MessageCreated(MessageCreateEventArgs message)
        {
            if (message.Author.IsBot)
                return;

            Client.DebugLogger.LogMessage(LogLevel.Debug, "GenjisBot", "message is null? " + (CustomResponses == null), DateTime.Now);
            await CustomResponses.ProcessReceivedMessage(message);
        }

        private Task Commands_CommandExecuted(CommandExecutionEventArgs e)
        {
            e.Context.Client.DebugLogger.LogMessage(LogLevel.Info, "Genjis Bot", $"{e.Context.User.Username} successfully executed '{e.Command.QualifiedName}'", DateTime.Now);
            return Task.CompletedTask;
        }

        private async Task Commands_CommandErrored(CommandErrorEventArgs e)
        {
            e.Context.Client.DebugLogger.LogMessage(LogLevel.Error, "Genjis Bot", $"{e.Context.User.Username} tried executing '{e.Command?.QualifiedName ?? "<unknown command>"}' but it errored: {e.Exception.GetType()}: {e.Exception.Message ?? "<no message>"}", DateTime.Now);

            if (e.Exception is ChecksFailedException ex)
            {
                var emoji = DiscordEmoji.FromName(e.Context.Client, ":no_entry:");
                
                var embed = new DiscordEmbedBuilder
                {
                    Title = "Access denied",
                    Description = $"{emoji} You do not have the permissions required to execute this command.",
                    Color = new DiscordColor(0xFF0000)
                };

                await e.Context.RespondAsync("", embed: embed);
            }
        }

        private async Task Client_Ready(ReadyEventArgs e)
        {
            DiscordEmbed discordEmbed = new DiscordEmbedBuilder
            {
                ImageUrl = "https://78.media.tumblr.com/80b033aebe301dc7e5135897dd5474a2/tumblr_p3vdeb4zEG1sq5ecpo4_400.gif",
            };

            foreach (KeyValuePair<ulong, DiscordGuild> g in Client.Guilds)
            {
                foreach (DiscordChannel c in await g.Value.GetChannelsAsync())
                    if (c.Name == "bot-testing-area")
                        await c.SendFileAsync("Resources/Ready.ogg", "Genji Bot está en linea.\nEscribe !help para ver una lista de comandos disponibles.", false, discordEmbed);
            }

            e.Client.DebugLogger.LogMessage(LogLevel.Info, "Genjis Bot", "Client is ready to process events.", DateTime.Now);
            DiscordGame botGame = new DiscordGame("How to Cook Humans");
            await Client.UpdateStatusAsync(botGame);
        }

        private Task Client_GuildAvailable(GuildCreateEventArgs e)
        {
            e.Client.DebugLogger.LogMessage(LogLevel.Info, "Genjis Bot", $"Guild available: {e.Guild.Name}", DateTime.Now);
            return Task.CompletedTask;
        }

        private Task Client_ClientError(ClientErrorEventArgs e)
        {
            e.Client.DebugLogger.LogMessage(LogLevel.Error, "Genjis Bot", $"Exception occured: {e.Exception.GetType()}: {e.Exception.Message}", DateTime.Now);
            return Task.CompletedTask;
        }
    }
}
