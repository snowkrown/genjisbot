﻿using DSharpPlus;
using DSharpPlus.EventArgs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace GenjisBot
{
    public class CustomResponses
    {
        private List<Response> allResponses = new List<Response>();

        private DiscordClient Client
        {
            get; set;
        }

        public CustomResponses (DiscordClient client)
        {
            Client = client;
        }

        public async Task ProcessReceivedMessage(MessageCreateEventArgs ctx)
        {
            if (await CheckSpecialWords(ctx))
                await Task.CompletedTask;

            await GetResponses();

            Response response = allResponses.Find(r => ctx.Message.Content.Contains(r.phrase));

            if (response == null || (response.activatorId > 0 && response.activatorId != ctx.Author.Id))
                await Task.CompletedTask;
            else
                await ReplyWithReplace(ctx, response.replace);
        }

        private static async Task ReplyWithReplace(MessageCreateEventArgs ctx, string response)
        {
            await ctx.Channel.TriggerTypingAsync();
            await Task.Delay(500);
            await ctx.Channel.SendMessageAsync(response);
        }

        public async Task GetResponses()
        {
            if (allResponses.Count > 0)
                await Task.CompletedTask;

            string json = "";

            using (var fs = File.OpenRead("Custom Responses/Words Database/ResponsesDatabase.json"))
            {
                using (var sr = new StreamReader(fs, new UTF8Encoding(false)))
                    json = await sr.ReadToEndAsync();
            }

            allResponses = JsonConvert.DeserializeObject<List<Response>>(json);
        }

        private async Task<bool> CheckSpecialWords(MessageCreateEventArgs message)
        {
            string lowerMessage = message.Message.Content.ToLower();

            if (lowerMessage.Contains("hola bot") || lowerMessage.Contains("hello bot") || lowerMessage.Contains("hi bot") || lowerMessage.Contains("saludos bot"))
            {
                await message.Channel.TriggerTypingAsync();
                await Task.Delay(500);
                int rnd = new Random().Next(0, 3);

                if (rnd == 0)
                    await message.Channel.SendFileAsync("Resources/Hello.ogg", $"Hola {message.Author.Mention}!");
                else if (rnd == 1)
                    await message.Channel.SendFileAsync("Resources/Greetings.ogg", $"Saludos {message.Author.Mention}!");
                else
                    await message.Channel.SendFileAsync("Resources/Yo.ogg", $"Hey {message.Author.Mention}!");

                return true;
            }

            return false;
        }
    }
}
